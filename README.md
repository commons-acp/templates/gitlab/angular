
# ![Angular Example App](logo.png)

# MY  APPLICATION WITH AUTNENTIFICATION AMPLIFY AND USE PRIMEENG ANGULAR

# Core /Api for Api of back
# Core /Service call APi and do Function
# Component juste call Service 

# use lazy Module (exp: list ...) and component( exp : add , update ....)   

# use Architecture
https://code-maze.com/angular-best-practices/


### Making requests to the backend API


If you want to change the API URL to a local server, simply edit `src/environments/environment.ts` and change `api_url` to the local server's URL (i.e. `localhost:3000/api`)


# Getting started

Make sure you have the [Angular CLI](https://github.com/angular/angular-cli#installation) installed globally. We use [Yarn](https://yarnpkg.com) to manage the dependencies, so we strongly recommend you to use it. you can install it from [Here](https://yarnpkg.com/en/docs/install), then run `yarn install` to resolve all dependencies (might take a minute).

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Building the project
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

#### Running unit tests
Run ng test to execute the unit tests via Karma.


