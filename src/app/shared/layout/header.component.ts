import { Component, OnInit } from '@angular/core';

import { User, UserService } from '../../core';
import {MegaMenuItem} from 'primeng/api';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  constructor(
    private userService: UserService
  ) {}

  currentUser: User;
  items: MegaMenuItem[];
  itemspublic: MegaMenuItem[];


  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );

/*
    this.items = [

      {
        label: 'Home', icon: 'pi pi-fw pi-video', routerLink:"/"

      },
      {
        label: 'Companies', icon: 'pi pi-fw pi-users',  routerLink:"/companies"

      },
      {
        label: 'Accounts', icon: 'pi pi-fw pi-users',  routerLink:"/accounts"

      },
      {
        label: 'Settings', icon: 'pi pi-fw pi-cog', routerLink :"/settings"

      },

    ];
    this.itemspublic = [
      {
        label: 'Home', icon: 'pi pi-fw pi-video', routerLink:"/"

      },
      {
        label: ' Sign in', icon: 'pi pi-fw pi-cog', routerLink :"/login"

      },
      {
        label: ' Sign up', icon: 'pi pi-fw pi-users',  routerLink:"/register"

      }

    ]*/





  }
}
