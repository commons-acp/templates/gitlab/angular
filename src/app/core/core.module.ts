import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTokenInterceptor } from './interceptors/http.token.interceptor';

import {
  ApiService,
  ArticlesService,
  AuthGuard,
  CommentsService,
  AmplifyService,
  ProfilesService,
  TagsService,
  UserService
} from './services';
import { UserApi} from './api/user.api';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    ApiService,
    ArticlesService,
    AuthGuard,
    CommentsService,
    AmplifyService,
    ProfilesService,
    TagsService,
    UserService,
    UserApi,
  ],
  declarations: []
})


export class CoreModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: CoreModule
    };
  }
}
