
function hello(name = 'world') {
  return `Hello ${name} 👋`;
}


function sayHello(name) {
  console.log(hello(name));
}

module.exports = {
  hello,
  sayHello,
};
