import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage';


if (environment.production) {
  enableProdMode();
}
fetch('./config/config.json').then(res => {


  res.clone().json().then(( result: any ) => {

    Auth.configure(result.amplify.Auth);
    Storage.configure(result.amplify.Storage);
    localStorage.setItem('BASE_URL', result.apiUrl);
    platformBrowserDynamic().bootstrapModule(AppModule)
      .catch(err => console.log(err));

  })
}) .catch((e) => {console.log("find config ")
  console.log(e);
});
